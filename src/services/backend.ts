import { createFetch } from '@vueuse/core'

export const backend = createFetch({
  baseUrl: import.meta.env.VITE_BACKEND_URL,
  options: {
    immediate: false
  },
  fetchOptions: {
    mode: 'cors',
    credentials: 'include'
  }
})
