import { createRouter, createWebHistory } from 'vue-router'

import HomeView from '@/views/home/HomeView.vue'
import FormSignup from '@/views/home/components/FormSignup.vue'
import FormLogin from '@/views/home/components/FormLogin.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      children: [
        {
          path: '',
          name: 'signup',
          component: FormSignup
        },
        {
          path: '/login',
          name: 'login',
          component: FormLogin
        }
      ]
    },
    // TODO: VER CUROS DO VUEMASTERY PARA PEGAR DICAS DE COMO PROTEGER ROTAS (USAR O USER EM VEZ DO TOKEN)
    // TODO: DICIDIR ENTRE CARREGAR OS DADOS ANTES OU DEPOIS DA ROTA SER CARREGADA
    {
      path: '/app',
      name: 'app',
      component: () => import('@/views/app/AppView.vue')
    }
  ]
})

export default router
